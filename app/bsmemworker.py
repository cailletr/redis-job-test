import os
import sys
import _thread
import json
import base64
import io
import subprocess as sp
import redisworker


def run_cmd(command):
    try:
        process = sp.Popen(command, stdout=sp.PIPE, shell=True)
        while True:
            line = process.stdout.readline().rstrip()
            if not line:
                break
            yield line
    except KeyboardInterrupt:
        sys.stdout.write('stop nicely')
        process.terminate()


# worker callback function
def run_bsmem(job):
    # we must use sys.stdout.write to avoid blanck line in redis log queue
    sys.stdout.write("processing job {}".format(job['id']))

    with open('input.fits', 'wb') as f:
        f.write(base64.b64decode(job['data']))
        f.close

    for path in run_cmd("bsmem -d input.fits -noui -clobber"):
        sys.stdout.write(str(path))

    with open("output.fits", "rb") as f:
        data = base64.b64encode(f.read()).decode('utf-8')

    return json.dumps({"id": job["id"],
                       "data": str(data)})


def cancel_job(message):
    sys.stdout.write(str(message))
    sys.stdout.write('cancel')
    _thread.interrupt_main()

if __name__ == "__main__":
    if 'RHOST' in os.environ:
        host = os.environ['RHOST']
    else:
        host = "localhost"

    if 'RPORT' in os.environ:
        port = os.environ['PORT']
    else:
        port = '6379'

    if 'CALLBAK' in os.environ:
        callback = locals()[os.environ['CALLBACK']]
    else:
        callback = run_bsmem

    worker = redisworker.RedisWorker(host, port, 'app1-jobs', run_bsmem,
                                     'app1-results', 'app1-log',
                                     'app1-cancel', cancel_job)
    worker.run()
