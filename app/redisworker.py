import redis
import json
import io
import sys
import socket


class RedisStdout(io.StringIO):
    def __init__(self, log_queue, redis_client):
        io.StringIO.__init__(self)
        self.log_queue = log_queue
        self.redis_client = redis_client
        self.stdout = sys.stdout

    def write(self, message):
        self.stdout.write('{}\n'.format(message))
        self.stdout.flush()
        self.redis_client.rpush(self.log_queue,
                                str(json.dumps({'host': socket.gethostname(),
                                                'message': message})))


class RedisWorker(object):
    def __init__(self, redis_host, redis_port,
                 jobs_queue, run_callback, results_queue, log_queue,
                 cancel_queue, cancel_callback):
        self.r = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
        self.run_callback = run_callback
        self.jobs_queue = jobs_queue
        self.results_queue = results_queue
        self.log_queue = log_queue
        self.cancel_channel = self.r.pubsub(ignore_subscribe_messages=True)
        self.cancel_channel.subscribe(**{cancel_queue: cancel_callback})
        self.cancel_thread = self.cancel_channel.run_in_thread(sleep_time=1)
        sys.stdout = RedisStdout(self.log_queue, self.r)

    def run(self):
        while(True):
            self.job = json.loads(self.r
                                  .blpop(self.jobs_queue)[1]
                                  .decode('utf-8'))
            result = self.run_callback(self.job)
            self.r.rpush(self.results_queue, result)
