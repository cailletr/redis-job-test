import redis
import time

r = redis.StrictRedis(host='localhost', port=6379, db=0)
while True:
    out = r.blpop('app1-results')[1].decode('utf8')
    if 'start_time' not in locals():
        start_time = time.time()
    print(time.time() - start_time)
