import redis
import json
import base64

r = redis.StrictRedis(host='localhost', port=6379, db=0)
out = r.blpop('app1-results')[1].decode('utf8')
result = json.loads(out)
image = base64.b64decode(result['data'])
with open('{id}'.format(id=result['id']), 'wb') as f:
    f.write(image)
    f.close
