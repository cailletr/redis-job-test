import redis
import json
import base64
import uuid
import sys

r = redis.StrictRedis(host='localhost', port=6379, db=0)
with open(sys.argv[1], "rb") as f:
    data = base64.b64encode(f.read()).decode('utf-8')
message = json.dumps({"id": str(uuid.uuid4()),
                      "data": str(data)})
r.rpush('app1-jobs', message)
