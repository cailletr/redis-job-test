import os
import sys
import _thread
import json
import base64
import io
import subprocess as sp
import redisworker

import PIL


# worker callback function
def pere_noel(job):
    # we must use sys.stdout.write to avoid blanck line in redis log queue
    sys.stdout.write("processing job {}".format(job['id']))

    image = PIL.Image.open(io.BytesIO(base64.b64decode(job['data'])))
    draw = PIL.ImageDraw.Draw(image)
    draw.text((0, 0), job['id'], (0, 0, 0))

    imgByteArr = io.BytesIO()
    image.save(imgByteArr, format='PNG')
    img = imgByteArr.getvalue()
    data = base64.b64encode(img).decode('utf-8')
    return json.dumps({"id": job["id"],
                       "data": str(data)})


def run_cmd(command):
    try:
        process = sp.Popen(command, stdout=sp.PIPE, shell=True)
        while True:
            line = process.stdout.readline().rstrip()
            if not line:
                break
            yield line
    except KeyboardInterrupt:
        sys.stdout.write('stop nicely')
        process.terminate()


def ping_google(job):
    for path in run_cmd("ping -c 50 google.com"):
        sys.stdout.write(str(path))


def cancel_job(message):
    sys.stdout.write(str(message))
    sys.stdout.write('cancel')
    _thread.interrupt_main()

if __name__ == "__main__":
    if 'RHOST' in os.environ:
        host = os.environ['RHOST']
    else:
        host = "localhost"

    if 'RPORT' in os.environ:
        port = os.environ['PORT']
    else:
        port = '6379'

    if 'CALLBAK' in os.environ:
        callback = locals()[os.environ['CALLBACK']]
    else:
        callback = ping_google

    worker = redisworker.RedisWorker(host, port, 'app1-jobs', ping_google,
                                     'app1-results', 'app1-log',
                                     'app1-cancel', cancel_job)
    worker.run()
